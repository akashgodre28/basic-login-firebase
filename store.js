import root from './reducer/index';

import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';

const composeEnhancer = compose;

const store = createStore(root, composeEnhancer(applyMiddleware(thunk)));

export default store;
