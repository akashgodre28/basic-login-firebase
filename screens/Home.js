import React, {useState, useEffect} from 'react';
import auth from '@react-native-firebase/auth';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Image,
  ScrollView,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import {SuccessBar, WarningBar} from '../components/CustomBars';
const {fontScale} = Dimensions.get('window');
const _width = Dimensions.get('screen').width;
const _height = Dimensions.get('screen').height;
import {signOut} from '../functions/users';
import Task from '../components/Task';
import {useSelector, useDispatch} from 'react-redux';
import {fetchtask} from '../actions/TasksActions';
import Activity from '../components/Activity';
import {startLoading, stopLoading} from '../actions/LoadingActions';
const Home = ({navigation}) => {
  const dispatch = useDispatch();
  const {tasks} = useSelector(state => state.taskReducer);

  useEffect(() => {
    if (tasks.length == 0) {
      setCreateTask(true);
    }
    return () => {};
  }, []);

  const {loading} = useSelector(state => state);
  const [taskValue, setTaskValue] = useState('');
  const [description, setDescription] = useState('');
  const [createTask, setCreateTask] = useState(false);

  const create = (task, description) => {
    dispatch(startLoading());
    AsyncStorage.getItem('@token')
      .then(res => {
        var data = JSON.stringify({
          task,
          description,
        });

        var config = {
          method: 'post',
          url: 'https://todo-list-task-spring.herokuapp.com/task/add',
          headers: {
            Authorization: 'Bearer ' + res,
            'x-api-key': 'akash',
            'Content-Type': 'application/json',
          },
          data: data,
        };

        axios(config)
          .then(function (response) {
            SuccessBar(response.data.message);
            setCreateTask(false);
            setTaskValue('');
            setDescription('');
            dispatch(stopLoading());
            dispatch(fetchtask());
          })
          .catch(function (error) {
            dispatch(stopLoading());
          });
      })
      .catch(err => {
        dispatch(stopLoading());
      });
  };

  const setLoading = flag => {
    if (flag) {
      dispatch(startLoading());
    } else {
      dispatch(stopLoading());
    }
  };

  return (
    <View style={styles.container}>
      {loading ? <Activity /> : <></>}
      <Top signOut={signOut} navigation={navigation} />
      <View style={styles.bwContainer}>
        <Text style={styles.text1}>
          {createTask ? `Create Task` : `Daily Tasks`}
        </Text>
        <TouchableOpacity
          onPress={() => {
            setCreateTask(!createTask);
          }}
          style={styles.add}>
          <Image
            style={{height: '100%', width: '100%'}}
            source={
              createTask
                ? require('../assets/close.png')
                : require('../assets/plus.png')
            }
          />
        </TouchableOpacity>
      </View>
      <View style={styles.supportBox}></View>
      <View style={styles.bottom}>
        {createTask ? (
          <View style={{height: '100%', width: '100%'}}>
            <TextInput
              placeholderTextColor="#ecf0f1"
              onChangeText={text => {
                setTaskValue(text);
              }}
              value={taskValue}
              multiline={false}
              maxLength={250}
              placeholder={'Task'}
              style={[styles.input2, {marginTop: '10%'}]}
              onFocus={() => {}}
            />
            <TextInput
              onChangeText={text => {
                setDescription(text);
              }}
              value={description}
              placeholderTextColor="#ecf0f1"
              multiline={true}
              placeholder={'Decription'}
              maxLength={1500}
              style={[styles.input1]}
              onFocus={() => {}}
            />
            <TouchableOpacity
              style={styles.btn}
              onPress={() => {
                if (taskValue != '') {
                  create(taskValue, description);
                } else {
                  WarningBar('Enter Values Correctly');
                }
              }}>
              <Text style={styles.text3}>{`Save Task`}</Text>
            </TouchableOpacity>
          </View>
        ) : (
          <ScrollView
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{width: '100%'}}>
            {tasks.length > 0 ? (
              tasks.map((item, index) => {
                return <Task key={index} todo={item} setLoading={setLoading} />;
              })
            ) : (
              <></>
            )}
          </ScrollView>
        )}
      </View>
    </View>
  );
};

export default Home;

const Top = ({navigation}) => {
  return (
    <View style={styles.top}>
      <TouchableOpacity
        onPress={() => signOut(navigation)}
        style={styles.imgBox}>
        <Image
          style={styles.img}
          source={{
            uri: 'https://img.icons8.com/ios/100/000000/logout-rounded--v1.png',
          }}
        />
      </TouchableOpacity>
      <View style={styles.imgBox}>
        <Image
          style={styles.img}
          source={{
            uri: 'https://img.icons8.com/office/100/000000/user.png',
          }}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: '100%',
    backgroundColor: '#F1F4F6',
    width: '100%',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  img: {height: '50%', width: '50%'},
  top: {
    width: '100%',
    height: '8%',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  imgBox: {
    height: _width * 0.15,
    width: _width * 0.15,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },

  text1: {
    fontFamily: 'Nunito-ExtraBold',
    color: '#9EAFBF',
    textAlignVertical: 'center',
    fontSize: 34 / fontScale,
    marginHorizontal: '5%',
  },

  text2: {
    fontFamily: 'Nunito-ExtraBold',
    color: '#FFF',
    textAlignVertical: 'center',
    fontSize: 34 / fontScale,
    marginTop: '6%',
    marginHorizontal: '5%',
  },
  btn: {
    backgroundColor: '#ecf0f1',
    borderRadius: 10,
    marginTop: '6%',
    alignItems: 'center',
    justifyContent: 'center',
    height: _height * 0.06,
    minHeight: 48,
    marginVertical: '3%',
    marginHorizontal: '5%',
    width: _width * 0.9,
  },
  input1: {
    textAlign: 'left',
    textAlignVertical: 'top',
    height: _height * 0.2,
    fontFamily: 'Nunito-SemiBold',
    width: _width * 0.9,
    fontSize: 18 / fontScale,
    marginHorizontal: '5%',
    marginTop: '6%',
    paddingHorizontal: '5%',
    color: '#FFF',
    borderWidth: 1,
    borderColor: '#ecf0f1',
    borderRadius: 10,
  },
  input2: {
    textAlign: 'left',
    textAlignVertical: 'center',
    height: _height * 0.06,
    fontFamily: 'Nunito-SemiBold',
    width: _width * 0.9,
    fontSize: 18 / fontScale,
    marginHorizontal: '5%',
    marginTop: '6%',
    paddingHorizontal: '5%',
    color: '#FFF',
    borderWidth: 1,
    borderColor: '#ecf0f1',
    borderRadius: 10,
  },
  text3: {
    fontFamily: 'Nunito-SemiBold',
    color: '#1D99FF',
    textAlignVertical: 'center',
    textAlign: 'center',
    fontSize: 18 / fontScale,
    marginHorizontal: '10%',
  },
  bwContainer: {
    width: '100%',
    height: '10%',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#FFF',
    borderTopRightRadius: 50,
  },
  supportBox: {
    width: '100%',
    height: '10%',
    position: 'absolute',
    top: '18%',
    backgroundColor: '#FFF',
  },
  bottom: {
    width: '100%',
    height: '82%',
    backgroundColor: '#1D99FF',
    borderTopRightRadius: 50,
  },
  add: {
    height: _height * 0.04,
    width: _height * 0.04,
    marginRight: '5%',
    textAlignVertical: 'center',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
