import React, {useState, useEffect, lazy} from 'react';

import {signInUser, signUpUser} from '../functions/users';

import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TextInput,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';
import {ErrorBar, SuccessBar, WarningBar} from '../components/CustomBars';
const {fontScale} = Dimensions.get('window');
const _width = Dimensions.get('screen').width;
const _height = Dimensions.get('screen').height;
import {useSelector, useDispatch} from 'react-redux';
import {startLoading, stopLoading} from '../actions/LoadingActions';
import Activity from '../components/Activity';

const Start = ({navigation}) => {
  const dispatch = useDispatch();
  const {loading} = useSelector(state => state);

  const setLoading = flag => {
    if (flag) {
      dispatch(startLoading());
    } else {
      dispatch(stopLoading());
    }
  };

  const [signIn, setSignIn] = useState(true);
  const [emailFoucs, setEmailFoucs] = useState(false);
  const [nameFocus, setNameFocus] = useState(false);
  const [passFocus, setPassFocus] = useState(false);

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [name, setName] = useState('');

  const emailInput = React.createRef();
  const nameInput = React.createRef();

  useEffect(() => {
    signIn ? emailInput.current.focus() : nameInput.current.focus();

    return () => {};
  }, [signIn]);

  return (
    <View style={styles.container}>
      {loading ? <Activity /> : <></>}
      <View style={styles.top}>
        <View style={styles.fig1}></View>
        <View style={styles.fig2}></View>
        <Text style={styles.text}>{`Create better\ntogether.`}</Text>
        <Text style={styles.text1}>{`Join our community.`}</Text>
      </View>
      <View style={styles.bottom}>
        {!signIn ? (
          <TextInput
            onChangeText={text => {
              setName(text);
            }}
            value={name}
            ref={nameInput}
            multiline={false}
            placeholder={'Full Name'}
            style={[
              styles.input,
              {marginTop: _height * 0.06},
              nameFocus ? {borderColor: '#000'} : {},
            ]}
            onFocus={() => {
              setNameFocus(true);
              setEmailFoucs(false);
              setPassFocus(false);
            }}
          />
        ) : (
          <></>
        )}

        <TextInput
          onChangeText={text => {
            setEmail(text);
          }}
          ref={emailInput}
          value={email}
          multiline={false}
          placeholder={'Email'}
          style={[
            styles.input,
            signIn ? {marginTop: _height * 0.06} : {},
            emailFoucs ? {borderColor: '#000'} : {},
          ]}
          onFocus={() => {
            setNameFocus(false);
            setEmailFoucs(true);
            setPassFocus(false);
          }}
        />
        <TextInput
          onChangeText={text => {
            setPassword(text);
          }}
          multiline={false}
          secureTextEntry
          value={password}
          placeholder={'Password'}
          style={[styles.input, passFocus ? {borderColor: '#000'} : {}]}
          maxLength={32}
          onFocus={() => {
            setNameFocus(false);
            setEmailFoucs(false);
            setPassFocus(true);
          }}
        />
        <TouchableOpacity
          style={styles.btn}
          onPress={() => {
            if (signIn) {
              if (email != '' && password != '') {
                signInUser(email, password, setLoading, navigation);
              } else {
                WarningBar('Please enter values correctly !! ');
              }
            } else {
              if (email != '' && password != '' && name != '') {
                signUpUser(email, password, name, setLoading);
              } else {
                WarningBar('Please enter values correctly !! ');
              }
            }
          }}>
          <Text style={styles.text2}>{signIn ? `Sign In` : `Sign Up`}</Text>
        </TouchableOpacity>

        {signIn ? <Text style={styles.text3}>Forget Password ?</Text> : <></>}
        <TouchableOpacity
          style={{
            zIndex: 3,
            position: 'absolute',
            bottom: _height * 0.04,
            height: _height * 0.1,
            width: '100%',
            alignSelf: 'center',
          }}
          onPress={() => {
            setSignIn(!signIn);
            setEmail('');
            setPassword('');
            setName('');
          }}>
          <Text style={styles.text4}>
            {signIn
              ? `Don't have an account? Sign up`
              : `Already have account? Sign in`}
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Start;

const styles = StyleSheet.create({
  container: {
    height: '100%',
    backgroundColor: '#FFFFFF',
    width: '100%',
    flex: 1,
  },
  top: {
    position: 'absolute',
    zIndex: 1,
    height: _height * 0.6,
    width: _width,
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: '#C9EEFF',
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  bottom: {
    position: 'absolute',
    zIndex: 2,
    top: _height * 0.45,
    height: _height * 0.55,
    width: _width,
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: '#FFFFFF',
    alignItems: 'flex-start',
    borderTopLeftRadius: 40,
    borderTopRightRadius: 40,
    justifyContent: 'flex-start',
  },

  text: {
    fontFamily: 'Nunito-ExtraBold',
    fontSize: 48 / fontScale,
    textAlign: 'left',
    marginHorizontal: '5%',
    lineHeight: 52,
    color: '#000',
  },
  text1: {
    fontFamily: 'Nunito-Bold',

    fontSize: 24 / fontScale,
    marginVertical: '8%',
    marginHorizontal: '5%',
  },

  text2: {
    fontFamily: 'Nunito-SemiBold',
    color: '#FFF',
    zIndex: 3,
    marginBottom: '2%',
    textAlignVertical: 'center',
    textAlign: 'center',
    fontSize: 18 / fontScale,
    marginHorizontal: '10%',
  },
  text3: {
    fontFamily: 'Nunito-Light',
    color: '#000',
    marginTop: '1%',
    fontSize: 18 / fontScale,
    marginHorizontal: '5%',
  },
  text4: {
    fontFamily: 'Nunito-Light',
    color: '#000',
    alignSelf: 'center',
    fontSize: 18 / fontScale,
  },
  input: {
    textAlign: 'left',
    height: _height * 0.06,
    fontFamily: 'Nunito-SemiBold',
    width: _width * 0.9,
    fontSize: 18 / fontScale,
    textAlignVertical: 'center',
    marginHorizontal: '5%',
    marginVertical: '3%',
    paddingHorizontal: '5%',
    color: 'black',
    borderWidth: 1,
    borderColor: '#dfe6e9',
    borderRadius: 10,
  },
  fig1: {
    position: 'absolute',
    top: -1 * _width * 0.2,
    right: -1 * _width * 0.2,
    borderRadius: 500,
    height: _width * 0.5,
    backgroundColor: '#0984e3',
    width: _width * 0.5,
  },

  btn: {
    backgroundColor: '#2455EF',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    height: _height * 0.06,
    minHeight: 48,
    marginVertical: '3%',
    marginHorizontal: '5%',
    width: _width * 0.9,
  },
  fig2: {
    position: 'absolute',
    top: _height * 0.3,
    left: -1 * _width * 0.2,
    borderRadius: 500,
    height: _width * 0.5,
    opacity: 0.5,
    backgroundColor: '#0984e3',
    width: _width * 0.5,
  },
});
