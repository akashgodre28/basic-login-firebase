import React, {useEffect} from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  ActivityIndicator,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
const {fontScale} = Dimensions.get('window');
const _width = Dimensions.get('screen').width;
const _height = Dimensions.get('screen').height;
import {fetchtask} from '../actions/TasksActions';
import {useDispatch} from 'react-redux';
import Activity from '../components/Activity';

const Splash = ({navigation}) => {
  const dispatch = useDispatch();
  useEffect(() => {
    const timer = setTimeout(() => {
      try {
        AsyncStorage.getItem('@isLogin').then(res => {
          const login = JSON.parse(res);

          if (login) {
            navigation.replace('Home');
            dispatch(fetchtask());
          } else {
            navigation.replace('Start');
          }
        });
      } catch (e) {
        console.log(e);
      }
    }, 1500);
    return () => clearTimeout(timer);
  }, []);

  return (
    <View style={styles.container}>
      <Activity />
      <Text style={styles.text1}>Basic TODO List</Text>
    </View>
  );
};

export default Splash;

const styles = StyleSheet.create({
  container: {
    height: '100%',
    backgroundColor: '#C9EEFF',
    width: '100%',
    justifyContent: 'flex-start',
  },

  text1: {
    fontFamily: 'Nunito-Bold',
    alignSelf: 'center',
    fontSize: 24 / fontScale,
    marginVertical: '8%',
    marginTop: '50%',
    marginHorizontal: '5%',
  },
});
