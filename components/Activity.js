import React from 'react';
import {View, ActivityIndicator, StyleSheet} from 'react-native';

const Activity = () => {
  return (
    <View style={styles.container}>
      <ActivityIndicator
        style={{
          zIndex: 1000,
        }}
        size={'large'}
        color={'green'}
      />
    </View>
  );
};

export default Activity;

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    alignItems: 'center',
    height: '100%',
    width: '100%',
    justifyContent: 'center',
    zIndex: 500,
  },
});
