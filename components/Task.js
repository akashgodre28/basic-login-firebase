import React from 'react';

import {
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  Image,
  StyleSheet,
} from 'react-native';
const {fontScale} = Dimensions.get('window');
const _width = Dimensions.get('screen').width;
import {useDispatch} from 'react-redux';
import {fetchtask} from '../actions/TasksActions';
import {updateTask, deleteTask} from '../functions/tasks';
const Task = ({todo, setLoading}) => {
  const checked = todo.isCompleted;
  const dispatch = useDispatch();

  const changeInRedux = () => {
    dispatch(fetchtask());
  };

  return (
    <View style={styles.container}>
      <View style={styles.sideBox}>
        <TouchableOpacity
          onPress={() => {
            updateTask(
              todo.taskId,
              !checked,
              todo.task,
              todo.description,
              setLoading,
              changeInRedux,
            );
          }}
          style={styles.box}>
          {checked ? <View style={styles.dot}></View> : <></>}
        </TouchableOpacity>
      </View>
      <View style={styles.taskBox}>
        <Text style={[styles.text2, checked ? styles.lineStroke : {}]}>
          {todo.task}
        </Text>
        {todo.description != '' &&
        todo.description != null &&
        todo.description != undefined ? (
          <Text style={[styles.text3, checked ? styles.lineStroke : {}]}>
            {todo.description}
          </Text>
        ) : (
          <></>
        )}
      </View>
      <TouchableOpacity
        onPress={() => {
          deleteTask(todo.taskId, setLoading, changeInRedux);
        }}
        style={styles.iconBox}>
        <Image
          style={{height: '70%', width: '70%'}}
          source={{
            uri: 'https://img.icons8.com/plasticine/50/000000/filled-trash.png',
          }}
        />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    justifyContent: 'flex-start',
    alignItems: 'center',
    display: 'flex',
    paddingVertical: '2%',
    flexDirection: 'row',
  },
  box: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    height: _width * 0.04,
    width: _width * 0.04,
    borderWidth: 2.5,
    borderRadius: 5,
    borderColor: '#FFF',
  },
  dot: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    height: _width * 0.02,
    width: _width * 0.02,
    backgroundColor: '#FFF',
    borderRadius: 8,
    borderColor: '#000',
  },
  img: {height: '50%', width: '50%'},
  top: {
    width: '100%',
    height: '8%',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  imgBox: {
    height: _width * 0.15,
    width: _width * 0.15,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },

  text1: {
    fontFamily: 'Nunito-Bold',
    color: '#9EAFBF',
    zIndex: 100,
    textAlignVertical: 'center',
    fontSize: 32 / fontScale,
    marginHorizontal: '5%',
  },

  text2: {
    fontFamily: 'Nunito-Bold',
    color: '#FFF',
    zIndex: 100,
    fontSize: 24 / fontScale,
  },
  text3: {
    fontFamily: 'Nunito-SemiBold',
    color: '#FFF',
    zIndex: 100,
    fontSize: 18 / fontScale,
  },
  lineStroke: {
    textDecorationLine: 'line-through',
    textDecorationStyle: 'double',
  },
  iconBox: {
    height: _width * 0.1,
    width: '10%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  taskBox: {
    width: '75%',
    margin: '2%',
    display: 'flex',
    justifyContent: 'center',
  },
  sideBox: {
    display: 'flex',
    width: '5%',
    marginLeft: '4%',
    marginRight: '2%',
  },
});

export default Task;
