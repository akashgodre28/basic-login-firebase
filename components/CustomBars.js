import Snackbar from 'react-native-snackbar';

export const ErrorBar = message => {
  Snackbar.show({
    text: message,
    duration: Snackbar.LENGTH_SHORT,
    textColor: 'black',
    backgroundColor: '#d64541',

    fontFamily: 'Nunito-SemiBold',
  });
};

export const SuccessBar = message => {
  Snackbar.show({
    text: message,
    duration: Snackbar.LENGTH_SHORT,
    textColor: 'black',
    backgroundColor: '#87d37c',
    fontFamily: 'Nunito-SemiBold',
  });
};

export const WarningBar = message => {
  Snackbar.show({
    text: message,
    duration: Snackbar.LENGTH_SHORT,
    textColor: 'black',
    backgroundColor: '#f5e653',
    fontFamily: 'Nunito-SemiBold',
  });
};
