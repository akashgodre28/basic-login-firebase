import React from 'react';

import {StatusBar} from 'react-native';

import 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Start from './screens/Start';
import Home from './screens/Home';
import Splash from './screens/Splash';
import store from './store';
import {Provider} from 'react-redux';

const Stack = createStackNavigator();

const App = () => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <StatusBar backgroundColor="#4a4a4a" barStyle="light-content" />
        <Stack.Navigator
          initialRouteName={'Splash'}
          screenOptions={{
            headerShown: false,
          }}>
          <Stack.Screen
            name="Splash"
            component={Splash}
            options={horizontalAnimation}
          />
          <Stack.Screen
            name="Start"
            component={Start}
            options={horizontalAnimation}
          />
          <Stack.Screen
            name="Home"
            component={Home}
            options={horizontalAnimation}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
};

export default App;

const horizontalAnimation = {
  cardStyleInterpolator: ({current, layouts}) => {
    return {
      cardStyle: {
        transform: [
          {
            translateX: current.progress.interpolate({
              inputRange: [0, 1],
              outputRange: [layouts.screen.width, 0],
            }),
          },
        ],
      },
    };
  },
};
