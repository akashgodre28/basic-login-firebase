import {fetchtask} from '../actions/TasksActions';
import {SuccessBar} from '../components/CustomBars';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';

export const updateTask = (
  taskId,
  isCompleted,
  task,
  description,
  setLoading,
  changeInRedux,
) => {
  setLoading(true);
  AsyncStorage.getItem('@token')
    .then(res => {
      var data = JSON.stringify({
        taskId,
        description,
        task,
        isCompleted,
      });
      var config = {
        method: 'put',
        url: 'https://todo-list-task-spring.herokuapp.com/task/update',
        headers: {
          Authorization: 'Bearer ' + res,
          'x-api-key': 'akash',
          'Content-Type': 'application/json',
        },
        data: data,
      };

      axios(config)
        .then(function (response) {
          changeInRedux();
          setLoading(false);
          SuccessBar(response.data.message);
        })
        .catch(function (error) {
          console.log(error);
          setLoading(false);
        });
    })
    .catch(err => {
      setLoading(false);
    });
};

export const deleteTask = (taskId, setLoading, changeInRedux) => {
  setLoading(true);
  AsyncStorage.getItem('@token')
    .then(res => {
      var config = {
        method: 'delete',
        url: `https://todo-list-task-spring.herokuapp.com/task/delete?taskId=${taskId}`,
        headers: {
          Authorization: 'Bearer ' + res,
          'x-api-key': 'akash',
          'Content-Type': 'application/json',
        },
      };

      axios(config)
        .then(function (response) {
          setLoading(false);
          changeInRedux();
          SuccessBar(response.data.message);
        })
        .catch(function (error) {
          setLoading(false);
        });
    })
    .catch(err => {
      setLoading(false);
    });
};
