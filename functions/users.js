import AsyncStorage from '@react-native-async-storage/async-storage';
import auth from '@react-native-firebase/auth';
import {SuccessBar, ErrorBar, WarningBar} from '../components/CustomBars';

export const signUpUser = (email, password, name, setLoading, navigation) => {
  setLoading(true);
  auth()
    .createUserWithEmailAndPassword(email, password)
    .then(res => {
      SuccessBar('Creating User on Firbase is Done');
      createUser(res.user.uid, name, email, password, setLoading, navigation);

      //Here we can use this id and other parameters and same this in DB
      //  Also name parament can be used later as per requirement

      console.log(res.user.uid);
      // we can map user with this id in my db
    })
    .catch(error => {
      setLoading(false);
      ErrorBar(error.message);
    });
};

export const signInUser = (email, password, setLoading, navigation) => {
  setLoading(true);
  auth()
    .signInWithEmailAndPassword(email, password)
    .then(res => {
      SuccessBar('Firebase Login Completed');
      generateToken(res.user.uid, email, password, setLoading, navigation);
      console.log(res.user.uid);
    })
    .catch(err => {
      setLoading(false);
      ErrorBar(err.message);
    });
};

export const createUser = (
  id,
  name,
  email,
  password,
  setLoading,
  navigation,
) => {
  var axios = require('axios');
  var data = JSON.stringify({
    id,
    fullName: name,
    email,
    password,
  });

  var config = {
    method: 'post',
    url: 'https://todo-list-task-spring.herokuapp.com/auth',
    headers: {
      'Content-Type': 'application/json',
    },
    data: data,
  };

  axios(config)
    .then(function (res) {
      setLoading(false);
      AsyncStorage.setItem('@isLogin', JSON.stringify(true));
      AsyncStorage.setItem('@token', res.data.token);
      navigation.replace('Home');
      SuccessBar('User account created & signed in!');
    })
    .catch(function (error) {
      setLoading(false);
      ErrorBar(error.message);
    });
};

export const generateToken = (id, email, password, setLoading, navigation) => {
  var axios = require('axios');
  var data = JSON.stringify({
    id,
    email,
    password,
  });

  var config = {
    method: 'post',
    url: 'https://todo-list-task-spring.herokuapp.com/auth',
    headers: {
      'Content-Type': 'application/json',
    },
    data: data,
  };

  axios(config)
    .then(function (res) {
      setLoading(false);
      AsyncStorage.setItem('@isLogin', JSON.stringify(true));
      AsyncStorage.setItem('@token', res.data.token);
      navigation.replace('Home');
      SuccessBar('Welcome !!');
    })
    .catch(function (error) {
      setLoading(false);
      ErrorBar(error.message);
    });
};

export const signOut = navigation => {
  auth()
    .signOut()
    .then(res => {
      navigation.replace('Start');
      AsyncStorage.removeItem('@isLogin');
      AsyncStorage.removeItem('@token');
      SuccessBar('User signed out!');
    })
    .catch(err => {
      ErrorBar(err.message);
    });
};
