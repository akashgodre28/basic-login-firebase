export const startLoading = () => {
  return {
    type: 'START',
  };
};

export const stopLoading = () => {
  return {
    type: 'STOP',
  };
};
