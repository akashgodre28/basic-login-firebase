import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

const FETCH_TASK_SUCCESS = 'FETCH_TASK_SUCCESS';
const FETCH_TASK_ERROR = 'FETCH_TASK_ERROR';

const fetchTaskSuccess = task => {
  return {
    type: FETCH_TASK_SUCCESS,
    payload: task,
  };
};

const fetchTaskError = () => {
  return {
    type: FETCH_TASK_ERROR,
  };
};

export const fetchtask = () => {
  return function (dispatch) {
    AsyncStorage.getItem('@token')
      .then(response => {
        var headers = {
          Authorization: 'Bearer ' + response,
          'x-api-key': 'akash',
          'Content-Type': 'application/json',
        };
        axios
          .get(`https://todo-list-task-spring.herokuapp.com/tasks`, {
            headers: headers,
          })
          .then(response => {
            dispatch(fetchTaskSuccess(response.data));
          })
          .catch(error => {
            dispatch(fetchTaskError());
          });
      })
      .catch(error => {
        dispatch(fetchTaskError());
      });
  };
};
