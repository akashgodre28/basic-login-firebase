import {combineReducers} from 'redux';

import {isLoading} from './LoadingReducer';
import {taskReducer} from './TaskReducers';
const root = combineReducers({
  loading: isLoading,
  taskReducer,
});

export default root;
