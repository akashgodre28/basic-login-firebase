const intitalTask = {
  tasks: [],
};

const FETCH_TASK_SUCCESS = 'FETCH_TASK_SUCCESS';
const FETCH_TASK_ERROR = 'FETCH_TASK_ERROR';

export const taskReducer = (state = intitalTask, action) => {
  switch (action.type) {
    case FETCH_TASK_SUCCESS:
      return (state = {
        tasks: action.payload.tasks,
      });
    case FETCH_TASK_ERROR:
      return (state = {
        tasks: [],
      });
    default:
      return state;
  }
};
